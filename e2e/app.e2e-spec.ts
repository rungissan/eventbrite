import { EventbritePage } from './app.po';

describe('eventbrite App', () => {
  let page: EventbritePage;

  beforeEach(() => {
    page = new EventbritePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
