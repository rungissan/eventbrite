import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';



const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PHONE_REGEX = /[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}/;
// Phone Number (Format: +99(99)9999-9999)
const PASS_REGEX = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
// Password (UpperCase, LowerCase and Number)

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  rForm: FormGroup;
  post: any;
  firstname: String = '';
  lastname: String = '';
  email: String = '';
  phone: String = '';
  password: String = '';
  company: String = '';
  floatingLabel: String = 'auto';
  hideRequiredMarker: Boolean = true;



  errorMessageExample1: string;
  errorMessageExample2: string;
  errorMessageExample3: string;



  constructor( ) {
     this.rForm = new FormGroup({

                 email: new FormControl('wee@ddh.com', Validators.compose([Validators.required,
                                                                    Validators.pattern(EMAIL_REGEX)
                                                                    ])),
                 password: new FormControl('111', Validators.compose([Validators.required,
                                                                       Validators.pattern(PASS_REGEX)
                                                                       ])),
                 firstname: new FormControl('First Name', Validators.compose([Validators.required,
                                                                        Validators.minLength(3),
                                                                        Validators.maxLength(50)
                                                                        ])),
                 lastname: new FormControl('Last Name', Validators.compose([Validators.required,
                                                                       Validators.minLength(2),
                                                                       Validators.maxLength(50)
                                                                       ])),
                phone: new FormControl('555', Validators.compose([Validators.required,
                                                                    Validators.pattern(PHONE_REGEX)
                                                                    ])),

                hideRequiredMarker: new FormControl(true),
                company: new FormControl()
    });
   }

  ngOnInit() {
  }
  addPost(post) {
    console.log(post);
   // this.name = post.name;
   // this.description = post.description;
  }

}



