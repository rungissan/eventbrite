import * as _ from 'lodash';
import {Pipe,PipeTransform} from '@angular/core';

@Pipe({
       name: 'dataFilter'
     })
export class DataFilterPipe implements PipeTransform {
     transform(array: any[], query: string):any {
       query = query.toLowerCase();
       console.log(' query : ' + query);
         if (query) {
          return _.filter(array, row => (((row.id.toString()).toLowerCase()).indexOf(query) > -1) ||
                                        (((row.subject__c.toString()).toLowerCase()).indexOf(query) > -1) ||
                                        (((row.startdatetime__c.toString()).toLowerCase()).indexOf(query) > -1) ||
                                        (((row.enddatetime__c.toString()).toLowerCase()).indexOf(query) > -1) );
        }
    return array;
  }
}
