import { Injectable, EventEmitter, Output } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/do';  // for debugging

/**
* This class provides the Data service with methods to read names and add names.
*/
@Injectable()
export class DataService {
   @Output() getLoggedInName: EventEmitter<any> = new EventEmitter(); 
   public apiUrl = 'https://quiet-journey-23086.herokuapp.com/api';
  // public apiUrl = 'http://localhost:4200';
    /**
    * Creates a new DataService with the injected Http.
    * @param {Http} http - The injected Http.
    * @constructor
    */
    constructor(private http: Http) {}
    loggedemit(){
    this.getLoggedInName.emit('Sign In');
    }
    /**
    * Returns an Observable for the HTTP GET request for the JSON resource.
    * @return {string[]} The Observable for the HTTP request.
    */
    get(url: string): Observable<string[]> {
        return this.http.get(this.apiUrl + url)
        .map((res: Response) => res.json())
                 //     .do(data => console.log('server data:', data))  // debug
        .catch(this.handleError);
    }

    /**
    * Handle HTTP error
    */
    private handleError (error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
    getlogin(): Observable<any[]> {
        return this.http.get('assets/login.json')
        .map((res: Response) => res.json())
        .catch(this.handleError);
    }

    login(body: Object): Observable<any[]> {
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.apiUrl + '/auth/sfdc', body, options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error: any) =>  Observable.throw(error.json().error || 'Server error')
                                               ); //...errors if any

    }

    logout(): Observable<any[]> {
        let body:string ='';
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers = new Headers({ 'Content-Type': 'application/json',
                                      'Authorization': 'Bearer ' + localStorage.getItem('auth')
                                    });
        let options = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post('', body, options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error: any) => Observable.throw(error.json().error || 'Server error')); //...errors if any

    }

    // Delete a object
    delete(url: string,id: string): Observable< any []> {
        return this.http.delete(`${this.apiUrl + url}/${id}`) // ...using put request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }



     // Add a new object
    add(url:String, body: any): Observable< String[]> {
        console.log(localStorage.getItem('oauth'));
        body.oauth = JSON.parse(localStorage.getItem('oauth'));
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.post(this.apiUrl + url, body, options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    // Update or Delete a object
    put(url: string, body: any): Observable<any[]> {
        console.log(localStorage.getItem('oauth'));
        body.oauth = JSON.parse(localStorage.getItem('oauth'));
        let bodyString = JSON.stringify(body); // Stringify payload
        let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        let options       = new RequestOptions({ headers: headers }); // Create a request option

        return this.http.put(`${this.apiUrl+url}/${body['id']}`, body, options) // ...using put request
                         .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

}
