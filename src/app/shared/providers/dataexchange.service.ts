import { Injectable } from '@angular/core';
@Injectable()
export class EventData {
    public id: number;
    public data: any;
    public constructor() { }
}
