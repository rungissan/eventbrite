import { Observable } from 'rxjs/Rx';
import { ConfirmDialogComponent } from '../components/dialog/confirm-dialog.component';
import { AttendeeRegComponent } from '../components/dialog/attendee-reg.component';
import { SessionsComponent } from '../components/dialog/sessions/sessions.component';
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable()
export class DialogsService {

    constructor(private dialog: MdDialog) { }

    public confirm(title: string, message: string): Observable<boolean> {

        let dialogRef: MdDialogRef<ConfirmDialogComponent>;

        dialogRef = this.dialog.open(ConfirmDialogComponent);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;

        return dialogRef.afterClosed();
    }
    public confirm_attendee(title: string, event: string, sessions: string[]): Observable<boolean> {

        let dialogRef: MdDialogRef<AttendeeRegComponent>;

        dialogRef = this.dialog.open(AttendeeRegComponent);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.event = event;
        dialogRef.componentInstance.sessions = sessions;
        return dialogRef.afterClosed();
    }
      public confirm_sessions(body): Observable<boolean> {

        let dialogRef: MdDialogRef<SessionsComponent>;

        dialogRef = this.dialog.open(SessionsComponent);
        dialogRef.componentInstance.isAdded = false;
        dialogRef.componentInstance.sessionid = body.id;
        dialogRef.componentInstance.title = body.title;
        dialogRef.componentInstance.event = body.eventid__c;

        if (body.id === '') {
                dialogRef.componentInstance.what_to_do = 'Add';
                dialogRef.componentInstance.momentStart = '';
                dialogRef.componentInstance.momentValue = '';
                dialogRef.componentInstance.subject = '';
                dialogRef.componentInstance.description = '';
                dialogRef.componentInstance.Name = '';
                           } else {
                              dialogRef.componentInstance.what_to_do = 'Update';
                              dialogRef.componentInstance.subject = body.subject__c;
                              dialogRef.componentInstance.description = body.description__c;
                              dialogRef.componentInstance.momentStart = body.startdatetime__c;
                              dialogRef.componentInstance.momentValue = body.enddatetime__c;;
                              dialogRef.componentInstance.Name = body.name;
                         }

        return dialogRef.afterClosed();
    }
}
