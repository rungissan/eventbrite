import { MdDialogModule, MdButtonModule  } from '@angular/material';
import { NgModule } from '@angular/core';
import { DialogsService } from './dialogs.service';

import { ConfirmDialogModule } from '../components/dialog/confirm-dialog.module';
import { AtendeeRegModule } from '../components/dialog/attendee-reg.module';
import { SessionsModule } from '../components/dialog/sessions/sessions.module';

@NgModule({
    imports: [
        MdDialogModule,
        MdButtonModule,
        ConfirmDialogModule,
        AtendeeRegModule,
        SessionsModule
    ],
    exports: [ ],
    declarations: [ ],
    providers: [DialogsService],
    entryComponents: [ ],
})
export class DialogsModule { }
