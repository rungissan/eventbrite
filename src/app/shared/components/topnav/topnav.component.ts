import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DataService } from '../../../shared/providers/data.service';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { ViewContainerRef } from '@angular/core';

@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit, OnDestroy {

   public Email: String;
   public Name: String;
   public Auth: String;
   public isLoggedString: String;
   myData: any;
   title: String = 'Event Schedule';
    constructor(
              private snackbar: MdSnackBar,
              public viewContainerRef: ViewContainerRef,
              private dataService: DataService
               ) {
       this.dataService.getLoggedInName.subscribe(res => this.changeName());
       this.Email = localStorage.getItem('email');
       this.Name = localStorage.getItem('name');
       this.Auth = localStorage.getItem('auth');

    }
    logoutnow() {
            localStorage.clear();
        }
    ngOnInit() {}
     ngOnDestroy() {
       this.dataService.getLoggedInName.unsubscribe();
     }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }
    private changeName(): void {
        console.log('пойман в топ');
       this.Email = localStorage.getItem('email');
       this.Name =  localStorage.getItem('name');
       this.Auth =  localStorage.getItem('auth');

    }
    isLogged(): boolean {
        this.Auth = localStorage.getItem('auth');
        if ( this.Auth !== null) { 
                                 return true;
                                  } else {
                                    return false; 
                                   }
    }
}
