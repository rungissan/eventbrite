import { NgModule } from '@angular/core';
import { AttendeeRegComponent } from './attendee-reg.component';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [
        AttendeeRegComponent
    ],
    declarations: [
        AttendeeRegComponent
    ],
    entryComponents: [
        AttendeeRegComponent
    ]
})
export class AtendeeRegModule { }
