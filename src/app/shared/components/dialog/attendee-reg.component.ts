import { DataService } from '../../providers/data.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PHONE_REGEX = /[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}/;
// Phone Number (Format: +99(99)9999-9999)


@Component({
    selector: 'app-attendee-dialog',
    templateUrl: './attendee-reg.component.html',
    styleUrls: ['./attendee-reg.component.scss']
})

export class AttendeeRegComponent implements OnInit {
 public title: string;
 public event: string;
 public sessions: string[];
  rForm: FormGroup;
  post: any;
  firstname: String = '';
  lastname: String = '';
  email: String = '';
  phone: String = '';
  password: String = '';
  company: String = '';
  floatingLabel: String = 'auto';
  hideRequiredMarker: Boolean = true;



  constructor(public dialogRef: MdDialogRef<AttendeeRegComponent>,
              private snackbar: MdSnackBar,
              private dataService: DataService ) {
   }

  ngOnInit() {
       this.rForm = new FormGroup({

                 email: new FormControl('', Validators.compose([Validators.required,
                                                                    Validators.pattern(EMAIL_REGEX)
                                                                    ])),
                 firstname: new FormControl('', Validators.compose([Validators.required,
                                                                        Validators.minLength(3),
                                                                        Validators.maxLength(50)
                                                                        ])),
                 lastname: new FormControl('', Validators.compose([Validators.required,
                                                                       Validators.minLength(2),
                                                                       Validators.maxLength(50)
                                                                       ])),
                  phone: new FormControl('', Validators.compose([Validators.required,
                                                                    Validators.pattern(PHONE_REGEX)
                                                                    ])),
                 eventid: new FormControl({value: this.event, disabled: false}),
                 sessions: new FormControl({value: JSON.stringify(this.sessions), disabled: false}),
                 company: new FormControl()
    });
  }
 addPost(post) {
  console.log(post);
       this.dataService.add('/attendee/', post)
                  .subscribe(
                       res => {
                               let config = new MdSnackBarConfig();
                                config.duration = 1000;
                                this.snackbar.open('New Attendee added', 'Ok', config);
                              },
                              error => { 
                                let config = new MdSnackBarConfig();
                                config.duration = 1000;
                                this.snackbar.open(error.message, 'Ok', config);
                                   }
                            );

    }

}
