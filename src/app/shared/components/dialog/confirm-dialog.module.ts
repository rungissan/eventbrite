import { NgModule } from '@angular/core';
import { LoginModule } from '../../../login/login.module';
import { ConfirmDialogComponent } from './confirm-dialog.component';

@NgModule({
    imports: [
        LoginModule
    ],
    exports: [
        ConfirmDialogComponent
    ],
    declarations: [
        ConfirmDialogComponent
    ],
    entryComponents: [
        ConfirmDialogComponent
    ]
})
export class ConfirmDialogModule { }
