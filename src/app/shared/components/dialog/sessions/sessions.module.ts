import { NgModule } from '@angular/core';
import { SessionsComponent } from './sessions.component';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateTimePickerModule } from 'ng-pick-datetime';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        DateTimePickerModule
    ],
    exports: [
        SessionsComponent
    ],
    declarations: [
        SessionsComponent
    ],
    entryComponents: [
        SessionsComponent
    ]
})
export class SessionsModule { }
