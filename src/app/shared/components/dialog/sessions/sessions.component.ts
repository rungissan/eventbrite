import { DataService } from '../../../providers/data.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as moment from 'moment';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const PHONE_REGEX = /[\+]\d{2}[\(]\d{2}[\)]\d{4}[\-]\d{4}/;
// Phone Number (Format: +99(99)9999-9999)


@Component({
    selector: 'app-sessions-dialog',
    templateUrl: './sessions.component.html',
    styleUrls: ['./sessions.component.scss']
})

export class SessionsComponent implements OnInit {
 public title: string;
 public event: string;
 public sessionid: string;
 public what_to_do: string;
 public isAdded:boolean;
 public description: string;
 public subject: string;
 public momentStart: string;
 public momentValue: string;
 public Name: string;

  rForm: FormGroup;
  post: any;
  firstname: String = '';
  lastname: String = '';
  email: String = '';
  phone: String = '';
  password: String = '';
  company: String = '';
  floatingLabel: String = 'auto';
  hideRequiredMarker: Boolean = true;


  constructor(public dialogRef: MdDialogRef<SessionsComponent>,
              private snackbar: MdSnackBar,
              private dataService: DataService ) {
   }

  ngOnInit() {
       this.rForm = new FormGroup({

                 description: new FormControl(this.description, Validators.compose([Validators.required,
                                                                        Validators.minLength(3),
                                                                        Validators.maxLength(250)
                                                                        ])),
                 eventid: new FormControl({value: this.event, disabled: false}),
                 title: new FormControl(this.subject, Validators.compose([Validators.required,
                                                                       Validators.minLength(2),
                                                                       Validators.maxLength(100)
                                                                       ])),
                 start: new FormControl(this.momentStart, [Validators.required]),
                 end:   new FormControl(this.momentValue, [Validators.required])
    });
  }
 addSession(post) {
  post['id'] = this.sessionid;
  post['Name'] = this.Name;
  console.log(post);
  let datetimeformatStart = moment(this.momentStart).format();
  let datetimeformatEnd = moment(this.momentStart).format();
  console.log(datetimeformatStart);
  console.log(datetimeformatEnd);
  post['start'] = datetimeformatStart;
  post['end'] = datetimeformatEnd;
   if (this.what_to_do === 'Add') {
      this.dataService.add('/sessions', post)
                  .subscribe(
                       res => {
                               let config = new MdSnackBarConfig();
                                config.duration = 1000;
                                this.snackbar.open('New Session added', 'Ok', config);
                                this.isAdded=true;
                              },
                              error => { 
                                let config = new MdSnackBarConfig();
                                config.duration = 1000;
                                this.snackbar.open(error.message, 'Ok', config);
                                   }
                            );
          } else {
                 this.dataService.put('/sessions', post)
                  .subscribe(
                       res => {
                               let config = new MdSnackBarConfig();
                                config.duration = 1000;
                                this.snackbar.open('New Session update', 'Ok', config);
                                this.isAdded=true;
                              },
                              error => { 
                                let config = new MdSnackBarConfig();
                                config.duration = 1000;
                                this.snackbar.open(error.message, 'Ok', config);
                                   }
                            );

          }
    }

}
