import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TopnavComponent } from './shared/components';
import { SidebarComponent } from './shared/components';

import { DialogsModule } from './shared/providers/dialogs.module';
import { DataService } from './shared/providers/data.service';
import 'hammerjs';

import { EventData } from './shared/providers/dataexchange.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    TopnavComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    FlexLayoutModule,
    DialogsModule,
    ReactiveFormsModule
     ],
  providers: [EventData,DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
