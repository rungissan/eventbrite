import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdCardModule } from '@angular/material';
import { ErrorPageRoutingModule } from './error-page-routing.module';
import { ErrorPageComponent } from './error-page.component';

@NgModule({
    imports: [
        CommonModule,
        ErrorPageRoutingModule,
        MdCardModule
    ],
    declarations: [ErrorPageComponent]
})
export class ErrorPageModule { }

