import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../shared/providers/data.service';
import { EventData } from '../../shared/providers/dataexchange.service';
import { TooltipPosition} from '@angular/material';
import { MdDialog, MdDialogRef, MdDialogConfig, MD_DIALOG_DATA} from '@angular/material';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { DialogsService} from '../../shared/providers/dialogs.service';
import { IDatatableSelectionEvent } from 'ng2-md-datatable';
import { Ng2FloatBtnComponent, Ng2FloatBtn } from '../../shared/others/ng2-float-btn';
import { Router } from '@angular/router';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { DataFilterPipe } from '../../shared/pipes/data-filter.pipe';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit, OnDestroy {

mainButton: Ng2FloatBtn;
buttons: Array<Ng2FloatBtn>;

private sub: any;
private parentRouteId: string;

myData: Array<any>;
totalRowsCount: number;
itemsPerPage: number = 50;
errorMessage: String;
position: string = 'before';
selectedValues: string[]=['first'];

public filterGroup:FormGroup;
public filterQuery2:string;
subscription: any;
emitter = new EventEmitter();
public result: Boolean;

public Auth: string;

    constructor(public dataService: DataService,
                public eventData: EventData,
                public dialog: MdDialog,
                public dialogsService: DialogsService,
                private snackbar: MdSnackBar,
                private route: ActivatedRoute,
                private router: Router,
                private dataFilterPipe: DataFilterPipe
                        )  {
         if ( this.eventData.data === undefined) {
                                     this.eventData.data = { name: '000000', title__c: 'Some Event',
                                      description__c: '<h3>Description</h3>', registration_limit__c: '0',
                                      remaining_seats__c: 0, status__c: 'Draft'};
                                      this.router.navigate(['']);
                                        }
          this.sub = this.route.queryParams.subscribe((params: any) => {
                 this.parentRouteId = params['id'];

        });
         this.selectedValues.pop();
         this.refreshData();
         this.mainButton = {
            color: 'accent',
            iconName: 'check'
        }
 
        this.buttons = [
            {
                color: 'accent',
                iconName: 'add',
                onClick: () => {
                    this.session_add_dialog();
                },
                label : 'Add'
            },
            {
                color: 'accent',
                iconName: 'remove',
                onClick: () => {
                    this.removeSession_dialog();
                },
                label : 'Remove'
            },
            {
                color: 'accent',
                iconName: 'update',
                onClick: () => {
                    this.session_update_dialog();
                },
                label : 'Update'
            }
        ]

    }

   ngOnInit() {
        this.filterQuery2 = '';
        this.filterGroup = new FormGroup({
                 filterQuery: new FormControl('', [Validators.required])
    });
}
   ngOnDestroy() {
        this.sub.unsubscribe();
        }
   attendee_dialog() {
           this.dialogsService
                  .confirm_attendee(this.eventData.data.title__c, this.parentRouteId, this.selectedValues)
                  .subscribe(res => this.result = res);
         }
    removeSession_dialog() {
          console.log(this.selectedValues);
          if ((this.selectedValues.length === 0) || (this.selectedValues.length > 1) ) {
              this.dialogsService
                  .confirm('Remove Session', 'Choose one session, please!')
                  .subscribe(res => this.result = res);
             }else {
                  this.dialogsService
                  .confirm('Remove Session', 'Are you sure you want to remove it?')
                  .subscribe(res =>{
                       this.result = res
                       console.log(this.result);
                       if (this.result) {
                       let body = {};
                       body['id'] = this.selectedValues[0];
                       let data_item =this.dataFilterPipe.transform(this.myData, this.selectedValues[0]);
                       console.log(data_item);
                       console.log(data_item[0]);
                       this.myData = this.myData.filter(item => item !== data_item[0]);

                       this.dataService.put('/sessions/delete', body )
                         .subscribe(
                                     res => {
                                        let config = new MdSnackBarConfig();
                                        config.duration = 5000;
                                        this.snackbar.open('session have been removed', 'Ok', config);
                                        let data_item =this.dataFilterPipe.transform(this.myData, this.selectedValues[0]);
                                        this.myData = this.myData.filter(item => item !== data_item);
                                         },
                               error =>  {
                                     let config = new MdSnackBarConfig();
                                     config.duration = 5000;
                                     this.snackbar.open(error.message, 'Ok', config);
                                 }
                       );
                              }
                });
             }
            }
      session_add_dialog() {
           let body = {};
                   body['id'] = '';
                   body['eventid__c'] = this.parentRouteId;
                   body['title'] = this.eventData.data.title__c;
           this.dialogsService
                  .confirm_sessions(body)
                  .subscribe(res => {
                      this.result = res;
                       if (this.result) { this.refreshData();}
                    });
         }
     session_update_dialog() {
          console.log(this.selectedValues);
          if ((this.selectedValues.length === 0) || (this.selectedValues.length > 1) ) {
              this.dialogsService
                  .confirm('Update Session', 'Choose one session, please!')
                  .subscribe(res => this.result = res);
             }else {
                   const findItem = this.myData.find(item => item.id === this.selectedValues[0]);
                   let body = findItem;
                   body['title'] = this.eventData.data.title__c;;
                   this.dialogsService
                             .confirm_sessions(body)
                             .subscribe(res => {
                                        this.result = res;
                                          if (this.result) { this.refreshData();}
                                               });
                       }
            }
   onNotify(arg: IDatatableSelectionEvent) {
       this.selectedValues = arg.selectedValues;
       console.log('я слушаю Вас!:' + this.selectedValues);
      };
  isLogged(): boolean {
        this.Auth = localStorage.getItem('auth');
        if ( this.Auth !== null) { 
                                 return true;
                                  } else {
                                    return false; 
                                   }
  }
  refreshData(){
      this.dataService.get(`/sessions/${this.parentRouteId}`)
        //  this.dataService.get(`/assets/sessions.json`)
                  .subscribe(
                       res => {
                       this.myData = res;
                       this.totalRowsCount = this.myData.length;
                       console.log('this.totalRowsCount :' + this.totalRowsCount);
                         },
                        error =>  {
                             let config = new MdSnackBarConfig();
                             config.duration = 5000;
                             this.snackbar.open(error.message, 'Ok', config);
                                 }
                       );
           }
}




