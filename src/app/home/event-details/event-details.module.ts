import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventDetailsComponent } from './event-details.component';
import { EventDetailsRoutingModule } from './event-details-routing.module';
import { DataService } from '../../shared/providers/data.service';
import { MaterialModule} from '@angular/material';
import { MdDataTableModule } from 'ng2-md-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2FloatBtnModule } from '../../shared/others/ng2-float-btn';
import {DataFilterPipe} from '../../shared/pipes/data-filter.pipe';

@NgModule({
  imports: [
    CommonModule,
    EventDetailsRoutingModule,
    MdDataTableModule,
    Ng2FloatBtnModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [EventDetailsComponent,DataFilterPipe],
  providers: [DataService, DataFilterPipe]
})
export class EventDetailsModule { }
