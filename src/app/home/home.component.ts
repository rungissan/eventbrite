
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../shared/providers/data.service';
import { EventData } from '../shared/providers/dataexchange.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  myData: Array<any>;
  // data table settings
    defaultRowsPerPage: number = 4;
    rowsPerPage:number[] = [4, 5, 10, 20, 100];
    i: Number;
    errorMessage: string;
  //  selectedValue: string;
    itemsPerPage: number;
    page: number;
    totalRowsCount: number;

 // add a property to the component

    constructor(public dataService: DataService,
                public eventData: EventData,
                public router: Router) {
     this.dataService.get('/events/')
       // this.dataService.get('/assets/data.json')
                  .subscribe(
                       res => {
                       this.myData = res;
                       this.totalRowsCount = this.myData.length;
                       this.dataService.loggedemit();
                         },
                        error =>  this.errorMessage = <any>error
                       );
    }

    // call the getnames function to fetch data from json
    ngOnInit() {
         this.rowsPerPage =  this.rowsPerPage || [];

         if(this.isEnabled()){
            this.defaultRowsPerPage = this.defaultRowsPerPage || this.rowsPerPage[0];
            this.setItemsPerPage(this.defaultRowsPerPage);
        }

    }

     goToDetails(item: any) {
      console.log(item);
      this.eventData.data = item;
      this.eventData.data.id = item.id;
      this.router.navigate(['/event-details'], { queryParams: { id: item.id }});
  }
    search(searchEvent) {
    let term = searchEvent.target.value;
    console.log('вася',term);
      }
    isEnabled(){
        return this.rowsPerPage.length > 0;
    }

    isActive(){
        return this.itemsPerPage > 0;
    }
    setItemsPerPage(pageSize: number){
        this.itemsPerPage = pageSize;
        this.page = 1;
    }

    getStartIndex(): number {
        return (this.page - 1 ) * this.itemsPerPage;
    }

    nextPage(): void{
        if(this.hasNextPage()){
            this.page++;
        }
    }

    previousPage(): void{
        if(this.hasPreviousPage()){
            this.page --;
        }
    }

    hasPreviousPage(): boolean {
        return this.page > 1;
    }

    hasNextPage(): boolean {
        let totalPages = Math.ceil(this.totalRowsCount / this.itemsPerPage);

        return this.page < totalPages;
    }
    isPreviousButtonDisabled(): string {
        if(this.hasPreviousPage() === false){
            return 'md-dark md-inactive';
        }

        return '';
    }

    isNextButtonDisabled(): string {
        if(this.hasNextPage() === false){
            return 'md-dark md-inactive';
        }

        return '';
    }

}
