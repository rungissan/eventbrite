import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: String = 'Event Schedule';
    constructor(
            private router: Router
    ) {  }
   // tslint:disable-next-line:use-life-cycle-interface
   ngOnInit () {
        /**
         * navigate to the login page when user land on root
         */
          //   this.router.navigate(['/home']);
    }
}
