import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from '../shared/providers/data.service';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { ViewContainerRef } from '@angular/core';

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  rForm: FormGroup;
  post: any;
  email: String = '';
  password: String = '';
  oath: String = '';
  myData: any;
  floatingLabel: String = 'auto';

  public Role: String;
  public Name: String;
  isLoggedString: String;
  constructor(private snackbar: MdSnackBar,
              public viewContainerRef: ViewContainerRef,
              private dataService: DataService,
              private router: Router ) {
       this.Role = localStorage.getItem('role');
       this.Name = localStorage.getItem('name');
       this.oath = localStorage.getItem('auth');
       if ((this.oath !== null )) {
                                  console.log('this.oath : ' + this.oath);
                                  localStorage.clear();
                                  this.router.navigate(['/home']);
                                 }
     this.rForm = new FormGroup({

                 email: new FormControl('', Validators.compose([Validators.required,
                                                                  Validators.pattern(EMAIL_REGEX)
                                                                 ])),
                 password: new FormControl('', Validators.required)
                   });
   }

  ngOnInit() {
  }
  addPost(post) {
  console.log(post);
       this.dataService.login(post)
                  .subscribe(
                       res => {
                               this.myData = res;
                               console.log('this.oath :' + JSON.stringify(this.myData.oauth_new));
                               localStorage.setItem('email', post.email);
                               localStorage.setItem('name', this.myData.response.name);
                               localStorage.setItem('auth', this.myData.oauth_new.access_token);
                               localStorage.setItem('oauth',JSON.stringify(this.myData.oauth_new));
                               this.router.navigate(['/home']);
                              },
                       error => {
                                let config = new MdSnackBarConfig();
                                config.duration = 5000;
                                this.snackbar.open('Error: invalid_grant - authentication failure', 'Ok', config);
                                   }
                            );

    }

}


